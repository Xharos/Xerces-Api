# Dalrai's Realm Client 

![Build Status](https://gitlab.com/Xharos/Xerces-Api/badges/master/build.svg)
Xerces is a Java Minecraft game layer.
This project contains the server API of Xerces.

# Getting started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.


### Prerequisites

What things you need to install the software and how to install them
* [GIT] is a version control system.
* [JDK] is the Java Development Kit, Xerces need version 8 or higher.

### Git Clone

git clone [url] [options]
```
$ git clone git@gitlab.com:xharos/Xerces-Api.git DirFoldername
```
You can now import this project with your favorite Java IDE. Make sure to import this project as Gradle Project!


# Deployment

Compile your modifications using gradle :
```
./gradlew <task> (on Unix-like platforms such as Linux and Mac OS X)
gradlew <task> (on Windows using the gradlew.bat batch file)
```


# Contributing 

Please read [CONTRIBUTING.md] for details on our code of conduct, and the process for submitting pull requests to us.

# Authors

* **Xharos**  - *Initial work*

# License

GNU v3 can be found [here]!



   [git]: <https://git-scm.com/book/en/v1/Getting-Started-Installing-Git>
   [jdk]: <http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html>
   [CONTRIBUTING.md]: <https://gitlab.com/Xharos/Xerces-Api/blob/master/CONTRIBUTING.md>
   [here]: <https://gitlab.com/Xharos/Xerces-Api/blob/master/COPYING>
