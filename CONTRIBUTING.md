# Contributing
Xerces - API is written in Java and uses gradle as its build tool.

# Requirements

* gradle
* Java 1.8

## Installing requirements

### Java

Follow the instructions for your platform.

### Gradle

The project uses the gradle wrapper to download the specified version of gradle.
The gradle wrapper is run by using the following command:

```bash
$ ./gradlew
```
Note: on windows the command to run is gradlew.bat rather than gradlew

## Coding guidelines

Adopting the [Google Java Style] with the following changes:

```
3
    A source file consists of, in order:

      * Package statement
      * Import statements
      * [Micro](https://twitter.com/MicroNotFound)
      * Exactly one top-level class
      
      Exactly one blank line separates each section that is present.

4.2
    Our block indent is +4 characters

4.4
    Our line length is 100 characters.

4.5.2
    Indent continuation of +4 characters fine, but I think
    IDEA defaults to 8, which is okay too.
```
Header for IDEA can be retrieve [here]!

# Submit Pull requests 


   [Google Java Style]: <https://google.github.io/styleguide/javaguide.html>
   [here]: <https://gitlab.com/Xharos/Xerces-Api/tree/master/HEADER.md>